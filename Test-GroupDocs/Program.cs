﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using GroupDocs.Comparison;
using GroupDocs.Comparison.Options;
using GroupDocs.Comparison.Result;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Test_GroupDocs
{
    class Program
    {
        static readonly string baseDir = AppDomain.CurrentDomain.BaseDirectory;
        static readonly string pathOutputDelete = $"{baseDir}\\Data\\outputDelete.docx";
        static readonly string pathOutputInsert = $"{baseDir}\\Data\\outputInsert.docx";
        static readonly string pathOutput = $"{baseDir}\\Data\\Output.docx";
        static readonly string pathOutputGroupsDoc = $"{baseDir}\\Data\\OutputGroupsDoc.docx";
        static readonly string pathOutputTemplate = $"{baseDir}\\Data\\OutputTemplate.docx";

        //static readonly string pathSource = $"{baseDir}\\Data\\old.docx";
        //static readonly string pathTarget = $"{baseDir}\\Data\\new.docx";

        //static readonly string pathSource = $"{baseDir}\\Data\\Word2003A4a.docx";
        //static readonly string pathTarget = $"{baseDir}\\Data\\Word2003A4b.docx";

        //static readonly string pathSource = $"{baseDir}\\Data\\文字３００００a.docx";
        //static readonly string pathTarget = $"{baseDir}\\Data\\文字３００００a2.docx";

        //static readonly string pathSource = $"{baseDir}\\Data\\文字３００００a-Old.docx";
        //static readonly string pathTarget = $"{baseDir}\\Data\\文字３００００a2 -New.docx";

        static readonly string pathSource = $"{baseDir}\\Data\\FontSizeTextColorCharacterBackgroundOld.docx";
        static readonly string pathTarget = $"{baseDir}\\Data\\FontSizeTextColorCharacterBackgroundNew.docx";

        //static readonly string pathSource = $"{baseDir}\\Data\\Table-Old.docx";
        //static readonly string pathTarget = $"{baseDir}\\Data\\Table-New.docx";

        static ChangeInfo[] changesDelete = null;
        static void Main(string[] args)
        {
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                Console.WriteLine("Test GroupsDoc");
                Console.WriteLine("Waitting.....");
                using (FileStream fileStream = File.OpenRead($"{baseDir}\\lic\\GroupDocs.Comparison.NET.lic"))
                {
                    License license = new License();
                    license.SetLicense(fileStream);
                }

                //CompareOriginalGroupsDoc(pathSource, pathTarget, pathOutputGroupsDoc);
                CompareCustom();

                Console.WriteLine("--Done--");
                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;
                Console.WriteLine($"Time: {ts.Minutes} minutes  {ts.Seconds} seconds");
                Console.WriteLine("Enter any character to exit...");
                Console.ReadKey();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
        }

        private static void CopyStyles(WordprocessingDocument source, WordprocessingDocument destination, string styleId)
        {
            var sourceStyles = source.MainDocumentPart.StyleDefinitionsPart.RootElement;
            var stylesExtract = sourceStyles.Descendants<Style>().Where(s => s.StyleId.Value == styleId);
            foreach (var styleExtract in stylesExtract)
            {
                var destinationStyles = destination.MainDocumentPart.StyleDefinitionsPart.RootElement;
                destinationStyles.AppendChild(styleExtract.CloneNode(true));
            }
        }

        private static void CompareOriginalGroupsDoc(string source, string target, string output)
        {
            using (Comparer comparer = new Comparer(source))
            {
                comparer.Add(target);
                var compareOptions = new CompareOptions()
                {
                    ShowDeletedContent = false,
                    ShowInsertedContent = true,
                    ShowOnlySummaryPage = false,
                    GenerateSummaryPage = false,
                    ExtendedSummaryPage = false,
                    MarkChangedContent = true,
                    MarkNestedContent = true,
                    DetectStyleChanges = true,
                    DiagramMasterSetting = new DiagramMasterSetting() { UseSourceMaster = true }
                };

                comparer.Compare(output, compareOptions);
                Process.Start(output);
            }
        }

        private static void CompareCustom()
        {
            Console.WriteLine("--Start CompareCustom()--");
            List<Task> tasks = new List<Task>
            {
                Task.Run(() =>
                {
                    CompareShowDeletedContent(pathSource, pathTarget, pathOutputDelete);
                }),
                Task.Run(() =>
                {
                    CompareShowInsertContent(pathSource, pathTarget, pathOutputInsert);
                })
            };

            Task.WaitAll(tasks.ToArray());

            ExportResultCompare();
            Console.WriteLine("--End CompareCustom()--");
        }

        private static void CompareShowDeletedContent(string source, string target, string output)
        {
            Console.WriteLine("--Start CompareShowDeletedContent()--");
            using (var comparer = new Comparer(source))
            {
                comparer.Add(target);
                var compareOptionsDelete = new CompareOptions()
                {
                    ShowDeletedContent = true,
                    ShowInsertedContent = false,
                    ShowOnlySummaryPage = false,
                    GenerateSummaryPage = false,
                    ExtendedSummaryPage = false,
                    DetectStyleChanges = true,
                };

                comparer.Compare(output, compareOptionsDelete);
                changesDelete = comparer.GetChanges().Where(i => i.Type == ChangeType.Deleted).ToArray();
            }
            Console.WriteLine("--End CompareShowDeletedContent()--");
        }

        private static void CompareShowInsertContent(string source, string target, string output)
        {
            Console.WriteLine("--Start CompareShowInsertContent()--");
            using (var comparer = new Comparer(source))
            {
                comparer.Add(target);
                var compareOptionsInsert = new CompareOptions()
                {
                    ShowDeletedContent = false,
                    ShowInsertedContent = true,
                    ShowOnlySummaryPage = false,
                    GenerateSummaryPage = false,
                    ExtendedSummaryPage = false,
                    DetectStyleChanges = true,
                };

                comparer.Compare(output, compareOptionsInsert);
            }
            Console.WriteLine("--End CompareShowInsertContent()--");
        }

        private static void ExportResultCompare()
        {
            Console.WriteLine("--Start ExportResultCompare()--");
            #region Extract document OutputInsert.docx
            // Open a WordprocessingDocument for editing using the filepath.
            var wordprocessingDocumentInsert = WordprocessingDocument.Open(pathOutputInsert, false);
            var bodyInsert = wordprocessingDocumentInsert.MainDocumentPart.Document.Body;
            var allElementInsert = bodyInsert.Elements();
            #endregion

            #region Extract document OutputDelete.docx
            // Open a WordprocessingDocument for editing using the filepath.
            var wordprocessingDocumentDelete = WordprocessingDocument.Open(pathOutputDelete, false);
            var bodyDelete = wordprocessingDocumentDelete.MainDocumentPart.Document.Body;
            var allElementDelete = bodyDelete.Elements();
            #endregion

            #region Clone document OutputTemplate.docx
            MemoryStream destination = new MemoryStream();
            using (FileStream source = File.Open(pathOutputTemplate, FileMode.Open))
            {
                // Copy source to destination.
                source.CopyTo(destination);
            }
            #endregion

            #region Read file old.docx
            var wordprocessingDocumentOld = WordprocessingDocument.Open(pathSource, false);
            var bodyOld = wordprocessingDocumentOld.MainDocumentPart.Document.Body;
            var allElementOld = bodyOld.Elements();
            #endregion

            using (var wordprocessingDocumentOutput = WordprocessingDocument.Open(destination, true))
            {
                var bodyOutput = wordprocessingDocumentOutput.MainDocumentPart.Document.Body;

                #region push content to Output.docx
                var tableOutput = bodyOutput.Elements<Table>().FirstOrDefault();
                if (tableOutput == null)
                    return;


                var cellLeft = tableOutput.Descendants<TableRow>().ToList()[1].Descendants<TableCell>().ToList()[0];
                var cellRight = tableOutput.Descendants<TableRow>().ToList()[1].Descendants<TableCell>().ToList()[1];
                cellLeft.RemoveAllChildren();
                cellRight.RemoveAllChildren();

                #region Left - NEW
                foreach (var elementInsert in allElementInsert)
                {
                    OpenXmlCompositeElement elementNew = null;
                    if (elementInsert.GetType() == typeof(Paragraph))
                    {
                        var pPr = elementInsert.Elements<ParagraphProperties>().FirstOrDefault();
                        if (pPr != null && pPr.ParagraphStyleId != null && !string.IsNullOrEmpty(pPr.ParagraphStyleId.Val))
                        {
                            string pStyleId = pPr.ParagraphStyleId.Val;
                            CopyStyles(wordprocessingDocumentInsert, wordprocessingDocumentOutput, pStyleId);
                        }

                        elementNew = new Paragraph(elementInsert.OuterXml);
                    }

                    if (elementNew != null)
                        cellLeft.AppendChild(elementNew);
                }

                #endregion Left - NEW

                #region Right - OLD
                foreach (var elementDelete in allElementDelete)
                {
                    OpenXmlCompositeElement elementNew = null;
                    if (elementDelete.GetType() == typeof(Paragraph))
                    {
                        if (changesDelete.Any(i => elementDelete.InnerText.Contains(i.Text)))
                        {
                            //character style has been deleted
                            var pPr = elementDelete.Elements<ParagraphProperties>().FirstOrDefault();
                            if (pPr != null && pPr.ParagraphStyleId != null && !string.IsNullOrEmpty(pPr.ParagraphStyleId.Val))
                            {
                                string pStyleId = pPr.ParagraphStyleId.Val;
                                CopyStyles(wordprocessingDocumentDelete, wordprocessingDocumentOutput, pStyleId);
                            }

                            elementNew = new Paragraph
                            {
                                InnerXml = elementDelete.InnerXml
                            };
                            cellRight.AppendChild(elementNew);
                            continue;
                        }

                        //character style has not been deleted
                        var paragraphOld = allElementOld.FirstOrDefault(p => p.InnerText.Equals(elementDelete.InnerText));
                        if (paragraphOld == null)
                        {
                            elementNew = new Paragraph
                            {
                                InnerXml = elementDelete.InnerXml
                            };
                            cellRight.AppendChild(elementNew);
                            continue;
                        }

                        var pragraphPrOld = paragraphOld.Elements<ParagraphProperties>().FirstOrDefault();
                        if (pragraphPrOld != null && pragraphPrOld.ParagraphStyleId != null && !string.IsNullOrEmpty(pragraphPrOld.ParagraphStyleId.Val))
                        {
                            //Clone old paragraphs style
                            string pStyleId = pragraphPrOld.ParagraphStyleId.Val;
                            CopyStyles(wordprocessingDocumentOld, wordprocessingDocumentOutput, pStyleId);
                            elementNew = new Paragraph
                            {
                                InnerXml = elementDelete.InnerXml
                            };
                            cellRight.AppendChild(elementNew);
                        }
                        else
                        {
                            //Clone old element runs style
                            var runOld = paragraphOld.Elements<Run>().ToList();
                            foreach (var run in runOld)
                            {
                                var rPr = run.Elements<RunProperties>().FirstOrDefault();
                                if (rPr == null || rPr.RunStyle == null || string.IsNullOrEmpty(rPr.RunStyle.Val))
                                {
                                    continue;
                                }

                                string runStyleId = rPr.RunStyle.Val;
                                CopyStyles(wordprocessingDocumentOld, wordprocessingDocumentOutput, runStyleId);
                            }

                            elementNew = new Paragraph
                            {
                                InnerXml = paragraphOld.InnerXml
                            };
                            cellRight.AppendChild(elementNew);
                            continue;
                        }
                    }

                    cellRight.AppendChild(elementDelete.CloneNode(true));
                }
                #endregion Right - OLD

                #endregion push content to Output.docx

                wordprocessingDocumentInsert.Close();
                wordprocessingDocumentDelete.Close();
                wordprocessingDocumentOld.Close();
                wordprocessingDocumentOutput.SaveAs(pathOutput).Close();
                File.Delete(pathOutputDelete);
                File.Delete(pathOutputInsert);
                Process.Start(pathOutput);
            }

            Console.WriteLine("--End ExportResultCompare()--");
        }
    }
}

